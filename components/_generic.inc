<?php

/**
 * @file
 * Generic helpers for Webform Triple-S module hooks implementations.
 */

/**
 * Generic helper for implementations of hook_webform_sss_describe_component_COMPONENT().
 */
function _webform_sss_webform_sss_describe_component_generic($component) {
  return array(array(
    'type' => 'CHARACTER',
    'name' => $component['form_key'],
    'label' => $component['name'],
    'size' => !empty($component['extra']['width']) ? $component['extra']['width'] : NULL,
  ));
}

/**
 * Generic helper for implementations of hook_webform_sss_component_is_exportable_COMPONENT().
 */
function _webform_sss_webform_sss_component_is_exportable_generic($component) {
  return TRUE;
}

/**
 * Implements hook_webform_sss_component_submission_value_COMPONENT().
 */
function _webform_sss_webform_sss_component_submission_value_generic($component, $submission) {
  if (isset($submission->data[$component['cid']]['value'])) {
    $value = $submission->data[$component['cid']]['value'][0];
  }
  else {
    $value = NULL;
  }
  return array($value);
}