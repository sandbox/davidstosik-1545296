<?php

/**
 * Triple-S V1.0 Definition File exporter.
 */
class webform_sss_v10_sss_exporter extends webform_sss_v10_exporter {

  /**
   * Constructor.
   */
  function webform_sss_v10_sss_exporter($options) {
    parent::__construct($options);
  }

  function bof(&$file_handle) {
    parent::bof($file_handle);

    $this->_write_keyword('SSS', NULL, FALSE, TRUE);
    $this->_write_keyword('VERSION', '1.0');
    $this->_write_keyword('DATE', format_date(time(), $this->options['date_format']), TRUE);
    $this->_write_keyword('TIME', format_date(time(), $this->options['time_format']), TRUE);
    $this->_write_keyword('ORIGIN', webform_sss_origin(), TRUE);
    global $user;
    $this->_write_keyword('USER', $user->name, TRUE);
    $this->_write_keyword('SURVEY', NULL, FALSE, TRUE);
    $this->_write_keyword('TITLE', $this->options['node']->title, TRUE);
    $this->_write_keyword('RECORD', 'A', FALSE, TRUE);
  }

  function eof(&$file_handle) {
    $this->_write_keyword('END', 'RECORD');
    $this->_write_keyword('END', 'SURVEY');
    $this->_write_keyword('END', 'SSS');
    parent::eof($file_handle);
  }

  function set_headers($filename) {
    parent::set_headers($filename);
    drupal_add_http_header('Content-Disposition', "attachment; filename=$filename.sss");
  }

  function add_row(&$file_handle, $data) {
    $this->_write_keyword('VARIABLE', $data['index'], FALSE, TRUE);
    $this->_write_keyword('NAME', $data['name'], TRUE);
    $this->_write_keyword('LABEL', $data['label'], TRUE);
    $this->_write_keyword('TYPE', $data['type']);
    if (isset($data['size'])) {
      if (is_array($data['size'])) {
        $size = $data['size']['min'] . ' TO ' . $data['size']['max'];
        $this->_write_keyword('SIZE', $size);
      }
      else {
        $this->_write_keyword('SIZE', $data['size']);
      }
    }
    if (isset($data['values'])) {
      $this->_write_keyword('VALUES', NULL, FALSE, TRUE);
      foreach ($data['values'] as $key => $value) {
        if ($this->options['select_keys']) {
          $label = $key;
        }
        else {
          $label = $value;
        }
        $this->_write_value($label, $data['index']);
      }
      $this->_write_keyword('END', 'VALUES');
    }
    $this->_write_keyword('END', 'VARIABLE');
  }

  /**
   * Helper function ensuring format and writing to file.
   */
  function _write_keyword($keyword, $value = '', $escape_value = FALSE, $has_end = FALSE) {
    if (empty($keyword)) {
      return;
    }

    $line = '';
    if ($keyword == 'END') {
      $this->indent--;
    }

    $line .= $keyword;
    if (!empty($value)) {
      $line .= ' ';
      if ($escape_value) {
        $line .= $this->_escape_string($value);
      }
      else {
        $line .= $value;
      }
    }

    $this->_write_line($line);

    if ($has_end) {
      $this->indent++;
    }
  }

  /**
   * Helper function ensuring format and writing to file.
   */
  function _write_value($value, $variable) {
    $index_cache = &drupal_static(__FUNCTION__);
    if (empty($index_cache[$variable])) {
      $index_cache[$variable] = 1;
    }
    $this->_write_line($index_cache[$variable] . ' ' . $this->_escape_string($value));
    $index_cache[$variable]++;
  }

  /**
   * Helper function escaping a string.
   */
  function _escape_string($string) {
    $replacements = array(
      '/\{/' => '{7B}',
      '/"/' => '{22}',
    );

    for ($car = 0; $car < 32; $car++) {
      $hex = (string)dechex($car);
      if (strlen($hex) == 1) {
        $hex = '0' . $hex;
      }
      $replacements['/\x' . $hex . '/'] = '{' . $hex . '}';
    }


    $string = '"' . preg_replace(array_keys($replacements), $replacements, $string) . '"';
    return $string;
  }

}