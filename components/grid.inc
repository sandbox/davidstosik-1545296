<?php

/**
 * @file
 * Implements hooks for Webform Triple-S module to export components of grid type.
 */

/**
 * Implements hook_webform_sss_describe_component_COMPONENT().
 */
function webform_sss_webform_sss_describe_component_grid($component) {
  $rows = array();
  $questions = _webform_select_options_from_text($component['extra']['questions'], FALSE, TRUE);
  $options = _webform_select_options_from_text($component['extra']['options'], FALSE, TRUE);
  foreach ($questions as $id => $question) {
    $rows[] = array(
      'type' => 'SINGLE',
      'name' => $component['form_key'] . '_' . $id,
      'label' => $component['name'] . ':' . $question,
      'values' => $options,
    );
  }
  return $rows;
}

/**
 * Implements hook_webform_sss_component_is_exportable_COMPONENT().
 */
function webform_sss_webform_sss_component_is_exportable_grid($component) {
}