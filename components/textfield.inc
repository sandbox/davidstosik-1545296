<?php

/**
 * @file
 * Implements hooks for Webform Triple-S module to export components of textfield type.
 */

/**
 * Implements hook_webform_sss_describe_component_COMPONENT().
 */
function webform_sss_webform_sss_describe_component_textfield($component) {
  module_load_include('inc', 'webform_sss', 'components/_generic');
  $rows = _webform_sss_webform_sss_describe_component_generic($component);
  return $rows;
}

/**
 * Implements hook_webform_sss_component_is_exportable_COMPONENT().
 */
function webform_sss_webform_sss_component_is_exportable_textfield($component) {
  if (empty($component['extra']['width'])) {
    return 'Maximum width must be set to be exportable by Webform Triple-S';
  }
  else {
    return TRUE;
  }
}

/**
 * Implements hook_webform_sss_component_submission_value_COMPONENT().
 */
function webform_sss_webform_sss_component_submission_value_textfield($component, $submission) {
  module_load_include('inc', 'webform_sss', 'components/_generic');
  return _webform_sss_webform_sss_component_submission_value_generic($component, $submission);
}