<?php

/**
 * @file
 * Implements hooks for Webform Triple-S module to export components of number type.
 */

/**
 * Implements hook_webform_sss_describe_component_COMPONENT().
 */
function webform_sss_webform_sss_describe_component_number($component) {
  module_load_include('inc', 'webform_sss', 'components/_generic');
  $rows = _webform_sss_webform_sss_describe_component_generic($component);
  $rows[0]['type'] = 'QUANTITY';
  // Calculate QUANTITY variable size.
  // $n is a shortcut just for readability convenience.
  $n = $component['extra'];
  $decimals = array();
  foreach (array('min', 'max', 'step') as $value) {
    //@FIXME is it safe to explode on '.', assuming the floating number is displayed with a point for decimals?
    $split = explode('.', $n[$value]);
    if (!empty($split[1])) {
      $decimals[$value] = strlen($split[1]);
    }
  }
  $precision = max($decimals);
  $rows[0]['size'] = array(
    'min' => number_format($n['min'], $precision, '.', ''),
    'max' => number_format($n['max'], $precision, '.', ''),
  );
  return $rows;
}

/**
 * Implements hook_webform_sss_component_is_exportable_COMPONENT().
 */
function webform_sss_webform_sss_component_is_exportable_number($component) {
  if (empty($component['extra']['min']) || empty($component['extra']['max']) || empty($component['extra']['step'])) {
    return 'Min, Max and Step values must be set to be exportable by Webform Triple-S';
  }
  else {
    return TRUE;
  }
}

/**
 * Implements hook_webform_sss_component_submission_value_COMPONENT().
 */
function webform_sss_webform_sss_component_submission_value_number($component, $submission) {
  module_load_include('inc', 'webform_sss', 'components/_generic');
  //@TODO format number value
  return _webform_sss_webform_sss_component_submission_value_generic($component, $submission);
}
