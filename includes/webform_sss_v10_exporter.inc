<?php

/**
 * Triple-S V1.0 exporter abstract class.
 * Centralises formatting helper methods.
 */
abstract class webform_sss_v10_exporter extends webform_exporter {
  var $options;
  var $indent;
  var $file_handle;

  /**
   * Constructor.
   */
  function webform_sss_v10_exporter($options) {
    $this->options = $options;
    $this->indent = 0;
  }

  function bof(&$file_handle) {
    parent::bof($file_handle);
    $this->file_handle = &$file_handle;
  }

  function eof(&$file_handle) {
    parent::eof($file_handle);
  }

  function set_headers($filename) {
    parent::set_headers($filename);
    drupal_add_http_header('Content-Type', 'application/force-download; charset=' . $this->options['encoding']);
  }

  /**
   * Helper function ensuring format and writing to file.
   */
  function _write_line($line) {
    // Indent line.
    $line = str_repeat(' ', $this->indent * 2) . $line;
    // Add carriage return.
    $line .= "\r\n";
    // Convert according to chosen encoding.
    $line = mb_convert_encoding($line, $this->options['encoding']);
    // Finally write line to file.
    @fwrite($this->file_handle, $line);
  }
}