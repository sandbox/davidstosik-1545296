<?php

/**
 * @file
 *   Provides Triple-S handlers for exporting webform results.
 */

/**
 * Implements hook_webform_exporters().
 */
function webform_sss_webform_exporters() {
  return array(
    'triple-s-1' => array(
      'title' => t('Triple-S V1.0'),
      'description' => t('Triple-S, The Survey Interchange Format Standard, Version 1.0'),
      'handler' => 'webform_sss_v10_asc_exporter',
      'definition_handler' => 'webform_sss_v10_sss_exporter',
    ),
  );
}

/**
 * Implements hook_date_format_types().
 */
function webform_sss_date_format_types() {
  return array(
    'webform_sss_date_time' => t('Webform SSS date and time'),
    'webform_sss_date' => t('Webform SSS date'),
    'webform_sss_time' => t('Webform SSS time'),
  );
}

/**
 * Implements hook_date_formats().
 */
function webform_sss_date_formats() {
  return array();/*
    array(
      'type' => 'webform_sss_date_time',
      'format' => 'Y/m/d H:i:s',
      #'locales' => array(),
    ),
    array(
      'type' => 'webform_sss_date',
      'format' => 'Y/m/d',
      #'locales' => array(),
    ),
    array(
      'type' => 'webform_sss_time',
      'format' => 'H:i:s',
      #'locales' => array(),
    ),
  );//*/
}

/**
 * Implements hook_webform_component_info_alter().
 * Add Webform Triple-S data to all supported components.
 */
function webform_sss_webform_component_info_alter(&$components) {
  foreach (module_implements('webform_sss_component_info') as $module) {
    $module_components = module_invoke($module, 'webform_sss_component_info');
    foreach ($module_components as $type => $info) {
      if (array_key_exists($type, $components)) {
        $components[$type]['webform_sss'] = array(
          'module' => $module,
          'file' => isset($info['file']) ? $info['file'] : NULL,
        );
      }
    }
  }
}

/**
 * Implements hook_webform_sss_component_info().
 */
function webform_sss_webform_sss_component_info() {
  $component_info = array(
    'date' => array(
      'file' => 'components/date.inc',
    ),
    'email' => array(
      'file' => 'components/email.inc',
    ),
    'grid' => array(
      'file' => 'components/grid.inc',
    ),
    /*
    'hidden' => array(
      'file' => 'components/hidden.inc',
    ),
    */
    'number' => array(
      'file' => 'components/number.inc',
    ),
    'select' => array(
      'file' => 'components/select.inc',
    ),
    'textarea' => array(
      'file' => 'components/textarea.inc',
    ),
    'textfield' => array(
      'file' => 'components/textfield.inc',
    ),
    'time' => array(
      'file' => 'components/time.inc',
    ),
  );
  return $component_info;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * FORM_ID = webform_results_download_form, results download page.
 */
function webform_sss_form_webform_results_download_form_alter(&$form, $form_state) {
  $exporters = webform_sss_webform_exporters();
  $visible_state = array();
  $selector = ':input[name="format"]';
  // We want some elements to be hidden when a Triple-S exporter is selected.
  // As conditions are ANDed, but we want to OR them, we negate each condition,
  // and use 'visible' insteas of 'invisible: A OR B = !(!A AND !B).
  // As all conditions are on the same element, we concatenate dummy data different for each one.
  foreach ($exporters as $key => $exporter) {
    $name = drupal_html_class($key);
    $visible_state[$selector . ',.dummy-' . $name] = array('!value' => $name);
  }
  $hidden_elements = array(
    &$form['delimiter'],
    &$form['select_options']['select_format'],
  );
  foreach ($hidden_elements as &$element) {
    $element['#states']['visible'] = $visible_state;
  }

  foreach ($form['node']['#value']->webform['components'] as $cid => $component) {
    $exportable = webform_sss_component_is_exportable($component);
    unset($message);
    if ($exportable === FALSE) {
      $message = 'Not supported by Webform Triple-S';
    }
    elseif (is_string($exportable)) {
      $message = $exportable;
    }
    if (isset($message)) {
      $form['components']['#options'][$cid] = t("!label <i>($message)</i>", array('!label' => $form['components']['#options'][$cid]));
    }
    else {

    }
  }

  $form['triple_s_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Triple-S options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => array(
      'invisible' => $visible_state,
    ),
  );

  $form['triple_s_options']['triple_s_encoding'] = array(
    '#type' => 'select',
    '#title' => t('Encoding'),
    '#options' => array(
      'ASCII' => t('ASCII (as specified by the format standard)'),
      'WINDOWS-1252' => t('WINDOWS-1252 (Ethnos compatible)'),
      'UTF-8' => t('UTF-8'),
    ),
    '#default_value' => 'ASCII',
  );

  $date_types = system_get_date_types();
  $date_format_options = array();
  foreach ($date_types as $key => $date_type) {
    $date_format_options[$key] = $date_type['title'];
  }

  $form['triple_s_options']['triple_s_date_time_format'] = array(
    '#type' => 'select',
    '#title' => t('Date and time string format'),
    '#options' => $date_format_options,
    '#default_value' => 'webform_sss_date_time',
  );

  $form['triple_s_options']['triple_s_date_format'] = array(
    '#type' => 'select',
    '#title' => t('Date string format'),
    '#options' => $date_format_options,
    '#default_value' => 'webform_sss_date',
  );

  $form['triple_s_options']['triple_s_time_format'] = array(
    '#type' => 'select',
    '#title' => t('Time string format'),
    '#options' => $date_format_options,
    '#default_value' => 'webform_sss_time',
  );

  // Alter submit button in case of Trible-S choice
  $form['submit']['#states'] = array(
    'invisible' => array(
      ':input[name="format"]' => array('value' => 'triple-s-1'),
    ),
  );
  $form['submit-asc'] = array(
    '#type' => 'submit',
    '#value' => t('Download .ASC Data file'),
    '#submit' => array('webform_sss_results_download_form_submit'),
    '#states' => array(
      'visible' => array(
        ':input[name="format"]' => array('value' => 'triple-s-1'),
      ),
    ),
  );

  $form['submit-sss'] = array(
    '#type' => 'submit',
    '#value' => t('Download .SSS Definition file'),
    '#submit' => array('webform_sss_results_download_form_submit'),
    '#states' => array(
      'visible' => array(
        ':input[name="format"]' => array('value' => 'triple-s-1'),
      ),
    ),
  );
}

/**
 * Form submit callback to download SSS Definition File or ASC Data file.
 */
function webform_sss_results_download_form_submit($form, $form_state) {

  $node = $form['node']['#value'];

  $options = array(
    'components' => webform_sss_filter_components(array_keys(array_filter($form_state['values']['components'])), $node),
    'select_keys' => $form_state['values']['select_keys'],
    'range_type' => $form_state['values']['range']['range_type'],
    'download' => $form_state['values']['download'],
    'node' => $node,
    'encoding' => $form_state['values']['triple_s_encoding'],
    'date_time_format' => $form_state['values']['triple_s_date_time_format'],
    'date_format' => $form_state['values']['triple_s_date_format'],
    'time_format' => $form_state['values']['triple_s_time_format'],
  );

  $clicked_btn_id = end($form_state['triggering_element']['#array_parents']);
  if ($clicked_btn_id == 'submit-sss') {
    $rows = _weform_sss_description_data($node, $options['components'], $options);
    $exporter = webform_sss_export_create_handler($form_state['values']['format'], $options, 'definition_handler');
  }
  else if ($clicked_btn_id == 'submit-asc') {
    // Retrieve the list of required SIDs.
    if ($options['range_type'] != 'all') {
      $options['sids'] = webform_download_sids($form_state['values']['node']->nid, $form_state['values']['range']);
    }
    else {
      $options['sids'] = NULL;
    }
    $rows = _weform_sss_submission_data($node, $options['sids'], $options['components'], $options);
    $exporter = webform_sss_export_create_handler($form_state['values']['format'], $options);
  }

  $file_name = drupal_tempnam('temporary://', 'webform_sss_');
  $handle = @fopen($file_name, 'w'); // The @ suppresses errors.
  $exporter->bof($handle);

  $row_count = 1;
  foreach ($rows as $row) {
    // Add a counter that will be the Triple-S variable id.
    $row['index'] = $row_count++;
    // Write data from component.
    $exporter->add_row($handle, $row);
  }

  // Add the closing bytes.
  $exporter->eof($handle);

  // Close the file.
  @fclose($handle);

  $export_info = array(
    'options' => $options,
    'file_name' => $file_name,
    'exporter' => $exporter,
    'row_count' => $row_count,
  );

  // If webform result file should be downloaded, send the file to the browser,
  // otherwise save information about the created file in $form_state.
  if ($options['download']) {
    // $exporter, $file_name, $row_count
    $export_name = _webform_safe_name($node->title);
    $export_info['exporter']->set_headers($export_name);
    @readfile($export_info['file_name']);  // The @ makes it silent.
    @unlink($export_info['file_name']);  // Clean up, the @ makes it silent.
    exit();
  }
  else {
    $form_state['export_info'] = $export_info;
  }
}

/**
 * Generate description data to be used by SSS definition file.
 */
function _weform_sss_description_data($node, $components, $options) {
  $rows = array();
  // Generate a row for each component.
  foreach ($components as $key) {
    if (is_int($key)) {
      // $key is a component id
      $component = $node->webform['components'][$key];
      $rows = array_merge($rows, webform_sss_describe_component($component, $options));
    }
    else {
      // $key is the name of a metadata attached to submissions
      // (serial, sid, time, draft, ip_address, uid, username)
      $row = webform_sss_describe_metadata($key, $options);
      if (!is_null($row)) {
        $rows[] = $row;
      }
    }
  }
  return $rows;
}

/**
 * Generate description data to be used by SSS definition file.
 */
function _weform_sss_submission_data($node, $sids, $components, $options) {
  // Get all the required submissions for the download.
  $filters = array('nid' => $node->nid);
  if (!empty($sids)){
    $filters['sid'] = $sids;
  }
  $submissions = webform_get_submissions($filters);

  $rows = array();
  foreach ($submissions as $submission) {
    $row = array();
    foreach ($components as $key) {
      if (is_int($key)) {
        // $key is a component id
        $component = $node->webform['components'][$key];
        $descriptions = webform_sss_describe_component($component, $options);
        $values = webform_sss_component_submission_value($component, $submission, $options);
        while (count($descriptions) && count($values)) {
          $row[] = array(
            'value' => array_shift($values),
            'description' => array_shift($descriptions),
          );
        }
      }
      else {
        // $key is the name of a metadata attached to submissions
        // (serial, sid, time, draft, ip_address, uid, username)
        $describe = webform_sss_describe_metadata($key, $options);
        if (!empty($describe)) {
          $row[] = array(
            'value' => webform_sss_submission_information_value($submission, $key, $options),
            'description' => $describe,
          );
        }
      }
    }
    $rows[] = $row;
  }

  return $rows;
}

/**
 * Instantiates a new Webform definition handler based on the format.
 */
function webform_sss_export_create_handler($format, $options, $key = 'handler') {
  if ($key == 'handler') {
    $handler = webform_export_create_handler($format, $options);
  }
  else {
    $definition = webform_export_fetch_definition($format);
    if (isset($definition) && class_exists($definition[$key])) {
      $handler = new $definition[$key]($options);
    }
    else  {
      // TODO: Create a default broken exporter.
      $handler = new webform_exporter_broken($options);
    }
  }

  return $handler;
}

/**
 * Returns an origin string describing the Drupal instance.
 */
function webform_sss_origin() {
  $origin = &drupal_static(__FUNCTION__);
  if (!isset($origin)) {
    $modules = system_list('module_enabled');
    $origin .= 'Drupal ' . $modules['system']->info['version'];
    $origin .= ', Webform ' . $modules['webform']->info['version'];
    $origin .= ', Webform Triple-S' . $modules['webform_sss']->info['version'];
  }
  return $origin;
}

/**
 * Describe a "submission information" field.
 */
function webform_sss_describe_metadata($key, $options) {
  $cache = &drupal_static(__FUNCTION__);
  if (!isset($cache[$key])) {
    // If getting submission number or ID, then use the biggest SID to get a size.
    if ($key == 'serial' || $key == 'sid') {
      $query = db_select('webform_submissions');
      $query->addExpression('MAX(sid)');
      $maxsid = $query->execute()->fetchField();
      $idsize = strlen($maxsid);
    }
    switch ($key) {
      case 'serial':
        $cache[$key] = array(
          'label' => t('Submission Number'),
          'name' => $key,
          'type' => 'CHARACTER',
          'size' => $idsize,
        );
        break;
      case 'sid':
        $cache[$key] = array(
          'label' => t('Submission ID'),
          'name' => $key,
          'type' => 'CHARACTER',
          'size' => $idsize,
        );
        break;
      case 'time':
        $cache[$key] = array(
          'label' => t('Time'),
          'name' => $key,
          'type' => 'CHARACTER',
          'size' => _webform_sss_time_format_max_size($options['date_time_format']),
        );
        break;
      case 'draft':
        $cache[$key] = array(
          'label' => t('Draft'),
          'name' => $key,
          'type' => 'LOGICAL',
        );
        break;
      case 'ip_address':
        $cache[$key] = array(
          'label' => t('IP Address'),
          'name' => $key,
          'type' => 'CHARACTER',
          'size' => 15,
        );
        break;
      case 'uid':
        $query = db_select('users');
        $query->addExpression('MAX(uid)');
        $maxuid = $query->execute()->fetchField();
        $size = strlen($maxuid);
        $cache[$key] = array(
          'label' => t('User ID'),
          'name' => $key,
          'type' => 'CHARACTER',
          'size' => $size,
        );
        break;
      case 'username':
        $cache[$key] = array(
          'label' => t('Username'),
          'name' => $key,
          'type' => 'CHARACTER',
          'size' => 60,
        );
        break;
      case 'info':
        // No data, just a container.
        // No break intended.
      default:
        $cache[$key] = NULL;
        break;
    }
  }

  return $cache[$key];
}

/**
 * Returns a submission's information.
 */
function webform_sss_submission_information_value($submission, $key, $options) {
  $cache = &drupal_static(__FUNCTION__ . 'cache');
  $serial = &drupal_static(__FUNCTION__ . 'serial', 0);
  if (!isset($cache[$submission->sid][$key])) {

    // Mapping between form keys and submission object property keys.
    $mapping = array(
      'sid' => 'sid',
      'time' => 'submitted',
      'draft' => 'is_draft',
      'ip_address' => 'remote_addr',
      'uid' => 'uid',
      'username' => 'name',
    );

    switch ($key) {
      case 'serial':
        $cache[$submission->sid][$key] = ++$serial;
        break;
      case 'time':
        $cache[$submission->sid][$key] = format_date($submission->{$mapping[$key]}, $options['date_time_format']);
        break;
      default:
        if (isset($mapping[$key])) {
          $cache[$submission->sid][$key] = $submission->{$mapping[$key]};
        }
        else {
          $cache[$submission->sid][$key] = NULL;
        }
        break;
    }
  }

  return $cache[$submission->sid][$key];
}

/**
 * Load a component Webform Triple-S file into memory.
 */
function webform_sss_component_include($component_type) {
  static $included = array();

  // No need to load components that have already been added once.
  if (!isset($included[$component_type])) {
    $components = webform_components(TRUE);
    $included[$component_type] = TRUE;

    if (($info = $components[$component_type]['webform_sss']) && isset($info['file'])) {
      $pathinfo = pathinfo($info['file']);
      $basename = basename($pathinfo['basename'], '.' . $pathinfo['extension']);
      $path = (!empty($pathinfo['dirname']) ? $pathinfo['dirname'] . '/' : '') . $basename;
      module_load_include($pathinfo['extension'], $info['module'], $path);
    }
  }
}

/**
 * Returns the value of a component in a submission, with its attached description.
 */
function webform_sss_component_submission_value($component, $submission, $options) {
  return webform_sss_component_invoke('component_submission_value', $component, $submission, $options);
}

/**
 * Returns a component description fitted to the export.
 * @TODO support SSS multiple versions.
 */
function webform_sss_describe_component($component, $options) {
  return (array)webform_sss_component_invoke('describe_component', $component, $options);
}

/**
 * Tells if a component is exportable.
 * Returns TRUE, or a warning message if not exportable.
 * @TODO support SSS multiple versions.
 */
function webform_sss_component_is_exportable($component) {
  return webform_sss_component_invoke('component_is_exportable', $component);
}

/**
 * Calls a component's webform_sss hook.
 */
function webform_sss_component_invoke($hook, $component) {
  $components = webform_components();
  if (empty($components[$component['type']]['webform_sss'])) {
    // Unsupported component.
    return NULL;
  }
  webform_sss_component_include($component['type']);
  $full_hook = 'webform_sss_' . $hook . '_' . $component['type'];
  if (module_hook($components[$component['type']]['webform_sss']['module'], $full_hook)) {
    $args = func_get_args();
    unset($args[0]);
    return call_user_func_array($components[$component['type']]['webform_sss']['module'] . '_' . $full_hook, $args);
  }
  else {
    return NULL;
  }
}

/**
 * Filter components to keep only those that are exportable.
 */
function webform_sss_filter_components($cids, $node) {
  $filtered = array();
  foreach ($cids as $cid) {
    if (is_int($cid)) {
      if (webform_sss_component_is_exportable($node->webform['components'][$cid]) === TRUE) {
        $filtered[] = $cid;
      }
    }
    else {
      $filtered[] = $cid;
    }
  }
  return $filtered;
}

/**
 * Tries to calculate the max string size of a date type.
 */
function _webform_sss_time_format_max_size($date_type) {
  $sample = 'Wed, 20 Sep 2012 19:07:07';
  return strlen(format_date(strtotime($sample), $date_type));
}
