<?php

/**
 * @file
 * Implements hooks for Webform Triple-S module to export components of select type.
 */

/**
 * Implements hook_webform_sss_describe_component_COMPONENT().
 */
function webform_sss_webform_sss_describe_component_select($component) {
  module_load_include('inc', 'webform_sss', 'components/_generic');
  $rows = _webform_sss_webform_sss_describe_component_generic($component);
  $rows[0]['type'] = $component['extra']['multiple'] ? 'MULTIPLE' : 'SINGLE';
  $rows[0]['values'] = _webform_select_options_from_text($component['extra']['items'], FALSE, TRUE);
  return $rows;
}

/**
 * Implements hook_webform_sss_component_is_exportable_COMPONENT().
 */
function webform_sss_webform_sss_component_is_exportable_select($component) {
  module_load_include('inc', 'webform_sss', 'components/_generic');
  return _webform_sss_webform_sss_component_is_exportable_generic($component);
}

/**
 * Implements hook_webform_sss_component_submission_value_COMPONENT().
 */
function webform_sss_webform_sss_component_submission_value_select($component, $submission) {
  if (isset($submission->data[$component['cid']]['value'])) {
    $value = $submission->data[$component['cid']]['value'];
    if (!$component['extra']['multiple']) {
      $value = $value[0] ? $value[0] : NULL;
    }
  }
  else {
    $value = $component['extra']['multiple'] ? array() : NULL;
  }
  return array($value);
}