<?php

/**
 * @file
 * Implements hooks for Webform Triple-S module to export components of time type.
 */

/**
 * Implements hook_webform_sss_describe_component_COMPONENT().
 */
function webform_sss_webform_sss_describe_component_time($component, $options) {
  module_load_include('inc', 'webform_sss', 'components/_generic');
  $rows = _webform_sss_webform_sss_describe_component_generic($component);
  $rows[0]['size'] = _webform_sss_time_format_max_size($options['time_format']);
  return $rows;
}

/**
 * Implements hook_webform_sss_component_is_exportable_COMPONENT().
 */
function webform_sss_webform_sss_component_is_exportable_time($component) {
  module_load_include('inc', 'webform_sss', 'components/_generic');
  return _webform_sss_webform_sss_component_is_exportable_generic($component);
}

/**
 * Implements hook_webform_sss_component_submission_value_COMPONENT().
 */
function webform_sss_webform_sss_component_submission_value_time($component, $submission, $options) {
  if (isset($submission->data[$component['cid']]['value'][0])) {
    $timestamp = strtotime($submission->data[$component['cid']]['value'][0]);
    $value = ($timestamp > 0) ? format_date($timestamp, $options['time_format']) : NULL;
  }
  else {
    $value = NULL;
  }
  return array($value);
}