<?php

/**
 * Triple-S V1.0 Data File exporter.
 */
class webform_sss_v10_asc_exporter extends webform_sss_v10_exporter {

  /**
   * Constructor.
   */
  function webform_sss_v10_asc_exporter($options) {
    parent::__construct($options);
  }

  function bof(&$file_handle) {
    parent::bof($file_handle);
  }

  function eof(&$file_handle) {
    parent::eof($file_handle);
  }

  function set_headers($filename) {
    parent::set_headers($filename);
    drupal_add_http_header('Content-Disposition', "attachment; filename=$filename.asc");
  }

  function add_row(&$file_handle, $data) {
    $line = '';
    foreach ($data as $field) {
      if (is_array($field) && array_key_exists('value', $field)) {
        $line .= $this->format_value($field['value'], $field['description']);
      }
    }
    $this->_write_line($line);
  }

  /**
   * Formats a single value, based on its field description.
   */
  function format_value($value, $description) {
    $formatted = '';
    switch ($description['type']) {
      case 'MULTIPLE':
        $bit_select = array_fill(1, count($description['values']), 0);
        foreach ($value as $selected) {
          $index = array_search($selected, array_keys($description['values'])) + 1;
          $bit_select[$index] = 1;
        }
        $formatted = implode($bit_select);
        break;
      case 'SINGLE':
        $size = strlen(count($description['values']));
        $index = array_search($value, array_keys($description['values']));
        $index = ($index === FALSE) ? '0' : $index + 1;
        $formatted = str_pad($index, $size, ' ');
        break;
      default:
        $formatted = (string)$value;
        if (isset($description['size']) && is_numeric($description['size'])) {
          $formatted = str_pad($formatted, $description['size'], ' ');
        }
        break;
    }
    return $formatted;
  }

}